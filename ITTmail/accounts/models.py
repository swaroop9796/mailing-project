from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save

# Create your models here.
class User_profile(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    Name = models.CharField(max_length=100, default='')
    city = models.CharField(max_length=100, default='')
    phone = models.CharField(max_length=13, default='')
    image = models.ImageField(upload_to='profile_image', blank=True)

    def __str__(self):
        return self.user.username

def create_profile(sender, **kwargs):
    if kwargs['created']:
        user_profile = User_profile.objects.create(user = kwargs['instance'])

post_save.connect(create_profile, sender=User)
