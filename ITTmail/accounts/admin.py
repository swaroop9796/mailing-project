from django.contrib import admin
from . import models

class User_profileAdmin(admin.ModelAdmin):
    list_display = ('user', 'city', 'phone')

admin.site.register(models.User_profile, User_profileAdmin)
admin.site.site_header = 'Administration'
# Register your models here.
